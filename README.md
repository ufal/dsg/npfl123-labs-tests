# NPFL123 Labs Tests

Runs some basic sanity checks for homework assignments.

For installation, best clone this repository and use from there. 
```
git clone https://gitlab.com/ufal/dsg/npfl123-labs-tests.git
```

Use with the same virtualenv/Conda environment as your main dialmonkey installation
(the checks will attempt to `import dialmonkey` to see if you have a working checkout).


To check `hw1`, run:

```
git pull
./run_tests.py hw1 path-to-your/dialmonkey-npfl123
```


Always update before running this, we're adding checks for new assignments as we go.
Some may only be available at the last minute, we're sorry for that!
