#!/usr/bin/env python3

from argparse import ArgumentParser
import sys
import importlib
from logzero import logger

from tests.common import git_checkout_branch, git_pull, git_check_date, check_file_presence


def main(hw_id, path):

    logger.info(f'Checking {hw_id} in {path}...')

    # checking path validity (will crash on fail)
    git_checkout_branch(hw_id, path)
    git_pull(path)

    sys.path.append(path)
    import dialmonkey

    test_module = importlib.import_module(f"tests.{hw_id}")

    # checking files and dates
    errors = 0
    errors += git_check_date(path, test_module.DEADLINE)
    errors += check_file_presence(path, test_module.FILES)

    # running actual checks
    errors += test_module.check(path)

    # report total number of errors/issues
    logger.info('Found %d issues.' % errors)


if __name__ == '__main__':

    ap = ArgumentParser(description='Running lab homework assignment tests.')

    ap.add_argument('hw_id', help='Assignment ID (hw1, hw2, etc.)',
                    choices=['hw1'])  # currently supported
    ap.add_argument('path', help='Path to your Dialmonkey-NPFL123 checkout')

    args = ap.parse_args()
    main(args.hw_id, args.path)
